package validate;

import entities.User;
import model.Users;
import repo.UserRepo;

import javax.servlet.http.HttpServlet;

public class LoginValidate {

    private Users users = Users.getInstance();



    public String validate(String loginOrEmail,String password){
        User user;
        if ((user = validateLoginOrEmail(loginOrEmail)) != null)
            if (validatePasswordMatch(user.getPassword(),password))
                return "Success";
            else return "InCorrect password";
        else return "InValid login or email";

    }


    private User validateLoginOrEmail(String loginOrEmail){
        UserRepo userRepo = new UserRepo();
        User user = userRepo.getByLoginOrEmail(loginOrEmail);
        if (user != null && (user.getLogin().equals(loginOrEmail) || user.getEmail().equals(loginOrEmail)))
        {
            return user;
        }
        return null;
    }

    private boolean validatePasswordMatch(String userPassword,String comparePassword){
        return userPassword.equals(comparePassword);

    }

}
