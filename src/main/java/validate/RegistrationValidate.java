package validate;

import entities.User;
import model.Users;
import repo.UserRepo;

public class RegistrationValidate {

    private Users users = Users.getInstance();


    public String validate(String name, String login, String email, String password, String confirmPassword) {
        if (validateInputs(new User(name, login, email, password))){
            if (validateEmail(email)){
                if (validateLogin(login)){
                    if (validatePassword(password,confirmPassword)){
                        return "Successful registration";
                    }else return "Passwords not match";
                }else return "Login already registered";
            }else return "Email already registered";
        }else return "Complete all inputs";
    }

    private boolean validateEmail(String email){
        UserRepo userRepo = new UserRepo();
        return userRepo.getByLoginOrEmail(email) == null;
    }

    private boolean validatePassword(String password, String repeatPassword){
        return password.equals(repeatPassword);
    }


    private boolean validateLogin(String login){
        UserRepo userRepo = new UserRepo();
        return userRepo.getByLoginOrEmail(login) == null;
    }

    private boolean validateInputs(User user){
        return user.getName().length() != 0 &&
                user.getEmail().length() != 0 &&
                user.getLogin().length() != 0 &&
                user.getPassword().length() != 0;

    }


}
