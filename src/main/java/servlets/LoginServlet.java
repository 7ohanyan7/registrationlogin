package servlets;

import entities.User;
import model.Users;
import repo.UserRepo;
import validate.LoginValidate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();

        String loginOrEmail = req.getParameter("loginOrEmail");
        String password = req.getParameter("password");

        LoginValidate validate = new LoginValidate();
        String info = validate.validate(loginOrEmail,password);
        System.out.println(info);
        if (info.equals("Success")){
            HttpSession session = req.getSession();
            UserRepo userRepo = new UserRepo();
            User user = userRepo.getByLoginOrEmail(loginOrEmail);
            session.setAttribute("name",user.getName());
            session.setAttribute("email",user.getEmail());
            session.setAttribute("login",user.getLogin());
            session.setAttribute("password",password);
            resp.sendRedirect("welcome");
        }else {
            req.setAttribute("error",info);
            req.getRequestDispatcher("views/login.jsp").include(req,resp);
        }



    }
}
