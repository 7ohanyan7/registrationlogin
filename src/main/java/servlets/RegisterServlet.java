package servlets;

import entities.User;
import model.Users;
import repo.UserRepo;
import validate.RegistrationValidate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class RegisterServlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String confirmPassword = req.getParameter("confirmPassword");

        RegistrationValidate validate = new RegistrationValidate();

        String info = validate.validate(name,login,email,password,confirmPassword);
        if (info.equals("Successful registration")){
            User user = new User(name, login, email, password);
            Users.getInstance().add(user);
            UserRepo userRepo = new UserRepo();
            userRepo.save(user);
            req.setAttribute("success",info);
            req.getRequestDispatcher("views/register.jsp").include(req,resp);
        } else {
            req.setAttribute("error",info);
            req.getRequestDispatcher("views/register.jsp").include(req,resp);
        }

    }
}
