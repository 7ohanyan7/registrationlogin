package servlets;

import entities.User;
import model.Users;
import repo.UserRepo;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserRepo userRepo = new UserRepo();
        List<User> users = userRepo.getUsers();
        req.setAttribute("users",users);

        req.getRequestDispatcher("views/list.jsp").forward(req,resp);
    }
}
