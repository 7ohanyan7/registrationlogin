package repo;

import entities.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepo {

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost/mydb?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    private static final String USER = "root";
    private static final String PASS = "root";

    private Connection connection;
    private PreparedStatement statement;

    public UserRepo(){
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL,USER,PASS);
            connection.setAutoCommit(false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void save(User user){
        try {
            statement = connection.prepareStatement("INSERT INTO mydb.users (name, login, email, password) VALUES (?, ?, ?, ?)");
            statement.setString(1,user.getName());
            statement.setString(2,user.getLogin());
            statement.setString(3,user.getEmail());
            statement.setString(4,user.getPassword());
            statement.executeUpdate();
            connection.commit();
            connection.close();

        } catch (SQLException e) {
            try {
                if (connection != null)
                    connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    public User getByLoginOrEmail(String loginOrEmail){
        User user = null;
        try {
            statement = connection.prepareStatement("SELECT name, login, email, password FROM mydb.users where login = ? or email = ?");
            statement.setString(1,loginOrEmail);
            statement.setString(2,loginOrEmail);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                 user = new User(
                    resultSet.getString(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public List<User> getUsers(){
        List<User> users = new ArrayList<User>();
        try {
            statement = connection.prepareStatement("SELECT name, login, email, password FROM mydb.users");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                users.add(new User(
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }
}
