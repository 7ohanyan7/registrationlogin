<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 7/16/2019
  Time: 3:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome</title>
</head>
<body>
    <font color="green">Welcome User <% out.println(session.getAttribute("name"));%></font><br>
    Your email is <% out.println(session.getAttribute("email")); %> <br>
    Your login is <% out.println(session.getAttribute("login")); %> <br>
    Your password is <% out.println(session.getAttribute("password")); %>
    <form action="${pageContext.request.contextPath}/logout" method="get">
        <input value="Logout" type="submit">
    </form>
</body>
</html>
