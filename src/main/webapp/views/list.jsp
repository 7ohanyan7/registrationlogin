<%@ page import="java.util.List" %>
<%@ page import="entities.User" %>
<%@ page import="java.io.PrintWriter" %><%--
  Created by IntelliJ IDEA.
  User: User
  Date: 7/15/2019
  Time: 2:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
</head>
<body>
    <ul>
        <%

            List<User> users = (List<User>) request.getAttribute("users");

            for (User user : users) { %>
               <li> <% out.println(user.toString()); %> </li>
        <%
            }
        %>
    </ul>

    <form action="${pageContext.request.contextPath}/home">
        <input type="submit" value="Home">
    </form>
</body>
</html>
