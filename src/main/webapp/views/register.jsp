<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 7/15/2019
  Time: 2:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
</head>
<body>
    <%
        if (request.getAttribute("success") != null){
           %> <font color='green'><b> <% out.println(request.getAttribute("success"));%></b></font>
    <% }
        else if (request.getAttribute("error") != null){
    %> <font color='red'><b> <% out.println(request.getAttribute("error"));%></b></font>
    <% }
    %>
    <form action="${pageContext.request.contextPath}/register" method="post">
        Name <br> <input type="text" name="name"><br>
        Login <br> <input type="text" name="login"><br>
        Email <br> <input type="text" name="email"><br>
        Password <br> <input type="password" name="password"><br>
        Confirm password <br> <input type="password" name="confirmPassword">
        <br> <input type="submit" value="Register">
    </form>
    <form action="${pageContext.request.contextPath}/home">
        <input type="submit" value="Home">
    </form>
</body>
</html>
