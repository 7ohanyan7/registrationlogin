<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 7/15/2019
  Time: 2:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
    <%
        if (request.getAttribute("error") != null)
        {
    %>
            <font color='red'><b> <% out.println(request.getAttribute("error"));%></b></font>
    <%
        }
    %>

    <form action="${pageContext.request.contextPath}/login" method="post">
        Login or email <br>
        <input type="text" name="loginOrEmail"> <br>
        Password <br>
        <input type="password" name="password">
        <br>
        <input type="submit" value="Login">
    </form>

    <form action="${pageContext.request.contextPath}/home">
        <input type="submit" value="Home">
    </form>
</body>
</html>
